#!/bin/bash

# The serial device. Please update this variable in both bg_router.sh and configure_router.sh.
SDEV=/dev/ttyS0

# Exit on error
set -e

function print_error_and_exit() {
	echo -e "\n\e[1;33m[$(date +%H:%M:%S)] \e[35m=> \e[31m[ERROR] ${1}\e[0m\n" >&2
	exit 1
}


function print_debug() {
	#if [ "${NETLAB_DEBUG}" = "true" ]; then
		echo -e "\e[1;33m[$(date +%H:%M:%S)] \e[35m=> \e[34m[DEBUG] \e[0m${1}\e[0m" >&2
	#fi
}



############### MAIN ##################

# Ensure this script is running as root.
if [ "$(whoami)" != "root" ]; then
	print_debug "Switching to root."
	sudo $0 $@
	exit 0
fi

# Ensure that exactly one bg_router.sh script instance is running.
NUM_BG_ROUTER="$(ps aux | grep 'bg_router\.sh' | grep -v grep | grep -v sudo | wc -l)"
print_debug "NUM_BG_ROUTER: $NUM_BG_ROUTER"
if [ $NUM_BG_ROUTER -lt 1 ]; then
	print_error_and_exit "You need to start bg_router.sh in a second terminal."
elif [ $NUM_BG_ROUTER -ne 3 ]; then
	print_error_and_exit "There is already one bg_router.sh script running."
fi


# Configure Serial Port
#stty -F ${SDEV} raw
stty -F ${SDEV} cs8 -cstopb kill ^U min 100 time 2 -hupcl brkint ignpar -icrnl -opost -onlcr -isig -icanon -echo  -parenb 9600



cat ${SDEV} | tee -a serial
#while [ 1 ]; 
#do
#    echo 'READING...'
#    READ=`dd if=${SDEV} count=22`
#    echo $READ
#done
#

