# Example Router Configuration

This example shows the configuration for Lab 3 Exercise 12 (ICMP Route Redirect).

1. Run `../bg_router.sh` in a second terminal.
2. Connect a router.
3. Run `../configure_router.sh`.
