# Networking Lab Tools

Tools and Scripts for a better and easier workflow in the Networking Lab.

## How-To: Configuring Routers

These steps should be executed on your laptop.

You need two open terminals. Both terminals **MUST** be in the directory of your current exercise!

### Terminal A

This Terminal will be used for running the background script.

Run the `bg_router.sh` script.

This script will continuously read the routers output and write it to stdout and the `serial` file.

If the script is already running in a different directory, you need to close the old script instance first, before running the new instance.


### Terminal B

This Terminal will be used for the actual configuration.

1. Add one or multiple `router_N.cfg` files. (`N` is the number of the router.)
2. Connect the router you want to configure to your laptop. Note: These scripts assume that the router is connected to `/dev/ttyS0`. You can change the serial port by updating the `SDEV` variable in both scripts.
3. Run the `configure_router.sh` script.

The `configure_router.sh` script will automatically detect the current router and 
1. reset the router,
2. go into the routers "Global configuration mode (RouterN(config)#)",
3. load the `router_N.cfg` into the router (lines beginning with `#` are ignored/comments), and
4. enable both interfaces.

Note: This script only works, if the `router_N.cfg` continuous being in the "Global configuration mode" after executing!

Note: If you encounter the `Unknown output: ` error message, you can try to re-run the script or reboot the router. Also check the output on Terminal A.

## Example

See [example/](example/).
