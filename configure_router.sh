#!/bin/bash

# The serial device. Please update this variable in both bg_router.sh and configure_router.sh.
SDEV=/dev/ttyS0
#SDEV=ttyS0

# Exit on error
set -e

function print_error_and_exit() {
	echo -e "\n\e[1;33m[$(date +%H:%M:%S)] \e[35m=> \e[31m[ERROR] ${1}\e[0m\n" >&2
	exit 1
}

function print_task() {
	echo -e "\e[1;33m[$(date +%H:%M:%S)] \e[35m=> \e[33m[TASK] \e[0m${1}\e[0m" >&2
}

function print_info() {
	echo -e "\e[1;33m[$(date +%H:%M:%S)] \e[35m=> \e[36m[INFO] \e[0m${1}\e[0m" >&2
}

function print_debug() {
	#if [ "${NETLAB_DEBUG}" = "true" ]; then
		echo -e "\e[1;33m[$(date +%H:%M:%S)] \e[35m=> \e[34m[DEBUG] \e[0m${1}\e[0m" >&2
	#fi
}

# Ensure that the router is in `Privileged EXEC mode (RouterN#)`.
# If the router is in another mode, it will automatically switch to the correct mode.
# Or if the router is in an unknown mode, it will throw an error.
#
# This will also set the `router` env variable, which contains the number of the router.
function orientate() {
	echo -e "\n\n\n\n" > ${SDEV}
	sleep 1
	line="$(cat serial | tail -n 1)"
	if echo "$line" | grep -q '^Router[0-9]>$' ; then
		print_task "Logging in..."
		echo "enable" > ${SDEV}
		sleep 1
		echo "lab" > ${SDEV}
		sleep 1
		print_info "Logged in."
		orientate
	elif echo "$line" | grep -q '^Router[0-9](config)#$' ; then
		print_task "Leaving config mode"
		echo "end" > ${SDEV}
		sleep 1
		orientate
	elif echo "$line" | grep -q '^Router[0-9]#$' ; then
		print_info "Everything is fine. Already logged in."
		router="$(echo "$line" | sed 's,^Router\([0-9]\)#$,\1,')"
	else
		print_error_and_exit "Unknown output: '${line}'"
	fi
}

function print_cmds() {
echo -e "\n\n\n"
cat - <<EOF
show ip route

clear ip route *

show ip cache

clear arp

show interface FastEthernet0/0
           


show interface FastEthernet0/1
           




configure terminal
no ip routing
ip routing

default interface FastEthernet0/0
default interface FastEthernet0/1

interface FastEthernet0/0
shutdown
no ip route-cache
ip route-cache
no shutdown
exit

interface FastEthernet0/1
shutdown
no ip route-cache
ip route-cache
no shutdown
exit
EOF

echo -e "\n\n\n"
cat "${routerfile}"
echo -e "\n\n\n"

cat - <<EOF


end

show ip interface FastEthernet0/0
                   


show ip interface FastEthernet0/1
                   


show ip route


EOF
echo -e "\n\n\n"
}




############### MAIN ##################


# Ensure this script is running as root.
if [ "$(whoami)" != "root" ]; then
	print_debug "Switching to root."
	sudo $0 $@
	exit 0
fi

# Ensure that exactly one bg_router.sh script instance is running.
NUM_BG_ROUTER="$(ps aux | grep 'bg_router\.sh' | grep -v grep | grep -v sudo | wc -l)"
print_debug "NUM_BG_ROUTER: $NUM_BG_ROUTER"
if [ $NUM_BG_ROUTER -lt 1 ]; then
	print_error_and_exit "You need to start bg_router.sh in a second terminal."
elif [ $NUM_BG_ROUTER -ne 2 ]; then
	print_error_and_exit "There are too many bg_router.sh script instances running."
fi

# Ensure both scripts are running in the same directory.
if [ ! -f "serial" ]; then
	print_error_and_exit "Missing 'serial' file. You must run bg_router.sh and configure_router.sh in the same directory!"
fi


# Configure Serial Port
#stty -F ${SDEV} raw
#stty -F ${SDEV} cs8 -cstopb -parenb 9600
#stty -F ${SDEV} cs8 -cstopb kill ^H min 100 time 2 -hupcl brkint ignpar -icrnl -opost -onlcr -isig -icanon -echo  -parenb 9600
stty -F ${SDEV} cs8 -cstopb kill ^U min 100 time 2 -hupcl brkint ignpar -icrnl -opost -onlcr -isig -icanon -echo  -parenb 9600

orientate
print_info "You are on Router '${router}'."
routerfile="router_${router}.cfg"
if [ ! -f "${routerfile}" ]; then
	print_error_and_exit "config file for router not found: ${routerfile}"
fi


print_task "Configuring..."

echo ""
print_cmds | grep -v '^#' > ${SDEV}
echo ""

print_info "Eventhough this script is done running now, the actual configuration might take another 10-15 seconds. Please see the output of the background script (bg_router.sh) in your second terminal and check that no errors occurred."


